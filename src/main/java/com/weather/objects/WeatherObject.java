package com.weather.objects;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class WeatherObject {

	String temp_C = "";
	String weatherDesc = "";
	String weatherIconUrl = "";
	
	String windspeedKmph = "";
	String humidity = "";
	String visibility = "";

	public WeatherObject(String xmlResponse) throws Exception{
		
		XMLParser parser = new XMLParser();
		Document docc = parser.loadXML(xmlResponse);
		
		Node resultNodes = docc.getDocumentElement();
		
		Node tempCNoce = parser.selectTheSingleNode(resultNodes, ".//temp_C");
		this.temp_C = tempCNoce.getNodeValue();
		
		Node weatherDescNode = parser.selectTheSingleNode(resultNodes, ".//weatherDesc");
		this.weatherDesc = weatherDescNode.getNodeValue();
		
		Node weatherIconUrl = parser.selectTheSingleNode(resultNodes, ".//weatherIconUrl");
		this.weatherIconUrl = weatherIconUrl.getNodeValue();
		
		Node windspeedKmphNode = parser.selectTheSingleNode(resultNodes, ".//windspeedKmph");
		this.windspeedKmph = windspeedKmphNode.getNodeValue();
		
		Node humidityNode = parser.selectTheSingleNode(resultNodes, ".//humidity");
		this.humidity = humidityNode.getNodeValue();
		
		Node visibilityNode = parser.selectTheSingleNode(resultNodes, ".//visibility");
		this.visibility = visibilityNode.getNodeValue();
		
	}
	
	public String getTempC(){
		return this.temp_C;
	}
	
	public String getWeatherDesc(){
		return this.weatherDesc;
	}
	
	public String getWeatherIconUrl(){
		return this.weatherIconUrl;
	}
	
	public String getWindSpeedKmph(){
		return this.windspeedKmph;
	}

	public String getHumidity() {
		return this.humidity;
	}

	public String getVisibility() {
		return this.visibility;
	}
	
	
}
